<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class MinecraftQueryException extends \Exception
{
	// Exception thrown by MinecraftQuery class
}