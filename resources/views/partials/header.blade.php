<nav class="main-nav">
    <ul class="nav-container">
        <li class="nav-item">
            <a href="{{ URL::to('/') }}">Home</a>
        </li>
        <li class="nav-item">
            <a href="/forum">Forums</a>
        </li>
        <li class="nav-item">
            <a href="/wiki">Wiki</a>
        </li>
        <li class="nav-item">
            <a class="supporter-link" href="http://store.craftblock.me">Store</a>
        </li>
    </ul>
</nav>

