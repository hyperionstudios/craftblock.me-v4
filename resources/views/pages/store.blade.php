@extends('base')
@section('header_styles')
    @parent
    @include('partials.inc.fontawesome')
@stop
@section('footer_scripts')
    @parent
    @include('partials.inc.angular')
@stop

@section('content')
    <div class="store container" ng-controller="storeCtrl">
        <iframe class="store-frame animated fadeInUp" src="//store.craftblock.me" frameborder="0"></iframe>
    </div><!-- /.store -->
@stop