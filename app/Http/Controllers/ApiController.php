<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class ApiController extends QueryController {

    public function __construct()
    {
    }
    /*
        @index
        Returns json with basic server information.  
     */
    public function index($ip, $port)
    { 
        $query = $this->getAll($ip, $port);
        return json_encode($query);
    }

    /*
        @show
        Returns json results with server.json config.
     */
    public function show($name)
    { 
        $config = file_get_contents(public_path() . '/servers.json', true);
        $config = json_decode($config, true);
        $ip = $config['base_ip'];
        $port = $config['servers'][$name]['port'];
        $query = $this->getAll($ip, $port);
        $config['servers'][$name]['state'] = $query;
        return json_encode($config['servers'][$name]);
    }
    /*
        @showAll
        Returns json results with server.json config for all servers.
     */
    public function showAll(){
        $config = file_get_contents(public_path() . '/servers.json', true);
        $config = json_decode($config, true);
        $ip = $config['base_ip'];
        foreach ($config['servers'] as $key => $value) {
            $port = $value['port'];
            $query = $this->getQuery($ip, $port);
            $queryInfo = $query->GetInfo();
            $value['state'] = $queryInfo;
            $value['state']['Player_List'] = $query->GetPlayers();
            $config['servers'][$key] = $value;
        }
        return json_encode($config);  
    }
    
    
    private function getQuery($ip, $port) {
        $query = new QueryController();
        $query->Connect($ip, $port, 10);
        return $query;
    }

    public function getAll($ip, $port){
        $query = $this->getQuery($ip, $port);
        return $query->GetInfo();
    }

    public function getPlayersList($ip, $port){
        $query = $this->getQuery($ip, $port);
        return $query->GetPlayers();
    }
    public function getOnlineSlots($ip, $port){
        $query = $this->getAll($ip, $port);
        return $query["Players"];
    }
    public function getMaxSlots($ip, $port){
        $query = $this->getAll($ip, $port);
        return $query["MaxPlayers"];
    }
    public function getPlugins($ip, $port, $view = "array"){
        $query = $this->getAll($ip, $port);
        if ($view == "array") {
           return $query["Plugins"];
        } else if ("list") {
            return $query["RawPlugins"];
        }
    }
    public function getVersion($ip, $port){
        $query = $this->getAll($ip, $port);
        return $query["Version"];
    }
    public function getSoftware($ip, $port){
        $query = $this->getAll($ip, $port);
        return $query["Software"];
    }
}
