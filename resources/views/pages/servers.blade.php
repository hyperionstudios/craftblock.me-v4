@extends('base')
@section('header_styles')
    @parent
    @include('partials.inc.fontawesome')
@stop
@section('footer_scripts')
    @parent
    @include('partials.inc.angular')
@stop

@section('content')
    <div class="servers-center container" ng-controller="serversCenterCtrl">
        <h2>The Servers Center</h2>
        <p>
            Heres some information on all of our online servers. Enjoy! <br>
            <small>Servers Center is a project we'll be expanding on. Stay tuned!</small>
        </p>
        <table class="table">
            <thead>
            <tr>
                <th>Server</th>
                <th>Ip Address</th>
                <th>Port</th>
                <th>Status</th>
                <th>Version</th>
                <th>Online Slots</th>
                <th>Max Slots</th>
            </tr>
            </thead>
            <tbody>
            <tr class="animated fadeIn [[ server.status ? '' : 'text-danger' ]]" ng-repeat="server in servers | orderObjectBy: 'id'">
                <td>[[ server.name  ]]</td>
                <td>[[ server.ip ]]</td>
                <td>[[ server.port ]]</td>
                <td>[[ server.statusDisplay ]]</td>
                <td>[[ server.version ]]</td>
                <td class="online-now">[[ server.online_now ]]</td>
                <td>[[ server.online_max ]]</td>
            </tr>
            </tbody>
        </table>
        <h3> Total Players Online </h3>
        <ul class="total-players-online" style="opacity: 1;">
            <li ng-repeat="player in total_players">
                <a href="#" class="has-tooltip">
                    <img class="animated bounceInUp" src="//mcapi.ca/skin/2d/[[ player ]]" width="42px" alt>
                    <span>[[ player ]]</span>
                </a>
            </li>
        </ul>
        <span class="total"></span>
    </div>
@stop