@extends('base')
@section('header_styles')
    @parent
    @include('partials.inc.fontawesome')
@stop
@section('footer_scripts')
    @parent
    @include('partials.inc.angular')
@stop

@section('content')
    <div class="about">
        <div class="logo-container animated">
            <img class="logo" src="{{ asset('res/images/logo.png') }}" alt>
        </div><!-- /.logo-container -->
        <div class="description-container">
            <div class="description container animated fadeIn">
                <p>
                    Survive, craft, and build on our servers, while having fun
                    with a large but amazing community. Come with us on
                    an adventure with CraftBlock, a Minecraft server network.
                </p>
            </div><!-- /.description -->
        </div>
    </div><!-- /.about -->
    <div class="servers container">
        <ul class="servers-container" ng-controller="serversCtrl">
            <li class="server" ng-repeat="server in servers | orderObjectBy: 'id'" >
                <header class="server-header">
                    <div class="server-title">
                        <h3 class="server-main-title">[[ server.name ]]</h3>
                        <h4 class="server-sub-title">[[ server.type ]]</h4>
                    </div><!-- /.server-title -->
                    <div class="server-state">
                        <div class="server-status">
                            <strong>Status: </strong><span>[[ server.statusDisplay ]]</span>
                        </div><!-- /.server-status -->
                        <div class="server-players">
                            <strong>Players: </strong><span>[[ server.state.Players ]]/[[ server.state.MaxPlayers ]] </span>
                        </div><!-- /.server-players -->
                    </div><!-- /.server-status -->
                </header>
                <div class="server-content">
                    <p>[[ server.description ]]</p>
                    <a class="server-link" href="[[ server.link_url ]]">[[ server.link_title ]] <i class="fa fa-arrow-right"></i></a>
                </div><!-- /.server-content -->
                <footer class="server-connection">
                    <div class="server-address">
                        [[ server.ip ]]
                    </div><!-- /.server-address -->
                    <span class="connection-content">[[ server.connection_content ]]</span>
                </footer>
            </li>
        </ul>
    </div><!-- /.servers -->
    <div class="modal fade" id="anime-flute" tabindex="-1" role="dialog" aria-labelledby="anime-fluteLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/s_m-Ez3W5h4" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
@stop