# CraftBlock Theme, v4. "A New Hope".

## Requirements:
### To use application:
1. Install Composer
2. In CraftBlock's directory, run `composer update` via command line.

### To edit CSS
1. Download and install Compass, which requires Ruby. `gem install compass`
2. Install `bootstrap-sass` gem. `gem install bootstrap-sass`
3. In `public/res/` run `compass watch` via command line.
4. You can now modify the files in `public/res/sass`.

### Using AngularJS
Angular script files are stored in a partials template in `resources/views/partials`. 

To include angularJS in a new template, include the following:

    @section('header_scripts')
        @parent
        @include('partials.angular')
    @show
    
All controllers are under the `cbApp` module and belong in `public/res/js/controllers.js`. To be honest, all angularJS logic is there. I'm lazy.


### AngularJS and Laravel's Delimiters
Blade and Angularjs both use the double curly braces delimiters. `{{ some.variable }}`

To avoid conflict, Angular's delimiters were changed to double brackets. `[[ some.angular.variable ]]`
