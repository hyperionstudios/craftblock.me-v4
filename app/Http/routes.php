<?php

Route::get('/', 'FrontpageController@index');
Route::get('/background.{ext?}', 'Controller@randBgImg');
Route::get('/servers', 'ServersController@index');
Route::get('/store', 'StoreController@index');

Route::get('/s/{code}', 'RouteController@redirect');
Route::get('/shorturls', 'RouteController@index');
Route::get('/shorturls/new', 'RouteController@new');
Route::get('/shorturls/edit', 'RouteController@edit');

Route::get('/servers/api/players/{ip}:{port}', function($ip, $port){
	$api = file_get_contents('http://api.minetools.eu/query/'. $ip .'/'. $port);
	print($api);
});

Route::get('/api/{ip}:{port}', 'ApiController@index');
Route::get('/api/s', 'ApiController@showAll');
Route::get('/api/s/{name}', 'ApiController@show');