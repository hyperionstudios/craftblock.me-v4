<div class="footer-copyright container">
	<div class="copyright"><strong>&copy; CraftBlock.me</strong></div><!-- /.copyright -->
	<div class="thanks">
		<small>
			Thanks to all of the players that comes to CraftBlock! <br>
			Nicole, Alyxia, asacavanagh for their help and support, Pickle15 for his amazing programming skills,
			Vanir for his amazing art, &amp; to the contributors who help pay for our expensive servers. Thank you!
		</small>
	</div>
</div><!-- /.thanks -->