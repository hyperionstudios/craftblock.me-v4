var cbApp = angular.module('cbApp', []);

cbApp.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

cbApp.controller('serversCtrl', function($scope, $http, serversService){
    $scope.servers;
    serversService.getServers().success(function(response){
        $scope.servers = response.servers;
        $('.servers').addClass('animated fadeIn');
    });
});

cbApp.service('serversService', function($http){
    this.getServers = function(){
        return $http.get(BASE_URL + '/api/s');
    }
});

cbApp.controller('serversCenterCtrl', function($scope, $http){
    f($scope, $http, $callback);
    $('.servers-center .table').addClass('animated fadeInUp');
});

cbApp.filter('orderObjectBy', function(){
    return function(input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for(var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function(a, b){
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return a - b;
        });
        return array;
    }
});

function f($scope, $http){
    $scope.servers = null;
    $http.get(BASE_URL + '/api/s').success(function(data){
        $scope.servers = data.servers;  
        angular.forEach($scope.servers, function(server) {
            server.status = true;
            server.statusDisplay = "Online!";
            server.online_now = server.state.Players;
            server.online_max = server.state.MaxPlayers;
            server.version = server.state.Version;
        });
    });
}

function fetchServersData($scope, $http, $callback) {
    $scope.servers = null;
    $http.get(BASE_URL +'servers.json').success(function(data){
        $scope.servers = data.servers;
        var base_ip = data.base_ip;
        $scope.total_players = [];
        angular.forEach(data.servers, function(server){
            var api = BASE_URL +'servers/api/players/' + base_ip + ':' + server.port;
            //var api = '//mcapi.us/server/status?ip=' + base_ip + '&port=' + server.port;
            $http.get(api).success(function(response){
                console.log(response);  
                if (response.error == undefined){
                    server.status = true;
                    server.statusDisplay = "Online!";
                    server.online_now = response.Players;
                    server.online_max = response.MaxPlayers;
                    server.version = response.Version;
                    
                    var playersOnline = response.Playerlist;
                    if (playersOnline != undefined && playersOnline != 'null') {
                        $scope.total_players = $scope.total_players.concat(playersOnline);
                    }
                    //console.log(playersOnline);
                    
                } 
                else {
                    server.status = false;
                    server.statusDisplay = "Offline :(";
                    server.online_now = 0;
                    server.online_max = 0;
                    server.version = 'N/A';
                }
            });
        });
        
    });
}



