<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class FrontpageController extends BaseController {



	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	public function index()
	{
		return view('frontpage.index');
	}

}
