@extends('base')
@section('header_styles')
    @parent
    @include('partials.inc.fontawesome')
@stop

@section('content')
    <img class="notfound animated zoomIn" src="{{ asset('res/images/404.png') }}" alt="Not found.">
@stop