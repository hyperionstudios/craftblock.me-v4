<?php

/*
 * Describe you custom columns and form items here.
 *
 * There is some simple examples what you can use:
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 * 		FormItem::register('otherCustomElement', function (\Eloquent $model)
 * 		{
 *			AssetManager::addStyle(URL::asset('css/style-to-include-on-page-with-this-element.css'));
 *			AssetManager::addScript(URL::asset('js/script-to-include-on-page-with-this-element.js'));
 * 			if ($model->exists)
 * 			{
 * 				return 'My edit code.';
 * 			}
 * 			return 'My custom element code';
 * 		});
 */

 Admin::model('CraftBlock\Models\ShortUrl')
    ->title('ShortUrls')
    ->columns(function ()
    {
        Column::string('code', 'Code');
        Column::string('name', 'Name');
        Column::string('url', 'Url');
        Column::date('created_at', 'Date created');
        Column::action('show', 'Link')->target('_blank')->icon('fa-link')->style('short')->url(function ($instance)
        {
            return URL::to('/s', array($instance->code));
        });
    })
    ->form(function ()
    {
        FormItem::text('code', 'Code');
        FormItem::text('name', 'Name');
        FormItem::textAddon('url', 'Url')->addon('http://')->placement('before');
    });
