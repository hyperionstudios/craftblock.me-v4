<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use CraftBlock\Models\ShortUrl;

class RouteController extends BaseController {

    public function redirect($code) {
        $sUrl = ShortUrl::where('code', '=', $code)->firstOrFail();
        $url = 'http://'. $sUrl->url;

        return redirect($url);
    }

}
