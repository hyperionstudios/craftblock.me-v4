<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Intervention\Image\Facades\Image;
use Log;

class Controller extends BaseController {

    /*
     * Returns a random Int from a files directory
     */
    public static function randomIntFromDirFiles($dir, $filetype) {
        $imageCount = count(glob($dir . '/*.' . '.' . $filetype));
        return rand(1, $imageCount);
    }
    
    public function randBgImg($extentision='all') {
        $ext = $this->validateExt($extentision);
        
        if ($extentision === 'all') {
            //search for all images
            $search = '*.{jpg,png,gif,bmp}';
        }
        else {
            //search for specific image
            $search = '*.{'. $ext .'}';
        }
        
        $dir = public_path() .'/res/images/backgrounds/';
        $images = glob($dir . $search, GLOB_BRACE);
        $index = mt_rand(0, count($images)-1);
        //var_dump($images);

        return Image::make($images[$index])->response(pathinfo($images[$index], PATHINFO_EXTENSION));
    }

    private function validateExt($ext) {
        switch ($ext) {
            case 'jpg':
                break;
            case 'jpeg':
                $ext = 'jpg';
                break;
            case 'png':
                break;
            case 'gif':
                break;
            case 'bmp':
                break;
            default:
                $ext = 'jpg'; //default to jpeg
        }
        return $ext;
    }
}
