$(function(){
	var logo = $('.logo');
	logo.on('mouseenter', function(){
		$(this).addClass('animated infinite pulse');
	}).on('mouseleave', function(){
		$(this).removeClass('animated infinite pulse');
	});
	logo.on('dblclick', function(){
		var fluteModal = $('#anime-flute');
		var fluteSrc = fluteModal.find('iframe').attr('src');
		fluteModal.find('iframe').attr('src', fluteSrc + '?autoplay=1&loop=1&rel=0&wmode=transparent');
		fluteModal.modal('show');
	});
    $('.store-frame').height($(window).height() - $('.main-header').height());
    $('.server-state').addClass('animated fadeIn online');
});