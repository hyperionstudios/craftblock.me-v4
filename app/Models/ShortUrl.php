<?php namespace CraftBlock\Models;

use Validator;
use SleepingOwl\Models\SleepingOwlModel;

class ShortUrl extends SleepingOwlModel {
    
    private static $chars = "abcdfghjkmnpqrstvwxyz123456789ABCDFGHJKLMNPQRSTVWXYZ";
    
	protected $fillable = array('code', 'name', 'url');
    
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            //if the code is empty, encode the id using base62
            if (empty($model->code)) {
                $model->code = $model->convertIdToShortCode($model->id);
                $model->save();
            }
        });
    }
    
    public function scopeDefaultSort($query)
    {
        return $query->orderBy('name', 'asc');
    }
    
    public function getValidationRules() {
        return array('code' => 'unique:short_urls,code',
            'name' => 'required|unique:short_urls,name|min:2',
            'url' => 'required');
    }
    

    /**
    * Note: this has a finite integer range (however, it's range is huge)
    */
    protected function convertIdToShortCode($id) {
        $id = (int)$id; //make sure it's an integer
        if ($id < 1) {
            throw new Exception("The ID is not a valid integer");
        }
 
        $length = strlen(self::$chars);
    
        $code = "";
        $codeLength = 4; //total length of the code
        for ($i=0; $i < $codeLength; $i++) {
            // determine the value of the next higher character
            // in the short code should be and prepend
            $code = self::$chars[(int)fmod($id, $length)] . $code;
            // reset $id to remaining value to be converted
            $id = (int)floor($id / $length);
        }
 
        return $code;
    }
}
