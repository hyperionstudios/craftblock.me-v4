<?php namespace CraftBlock\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class ServersController extends BaseController {



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('pages.servers');
    }
}
